import { Component, OnInit } from '@angular/core';

const todaysDogs =[
  {
    id: 1,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 2,
    name: 'Frankie',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/Frankie.png',
    breed: '',
    medicalNeeds: 'Hugs',
    userId: 1
  },
  {
    id: 3,
    name: 'Luna',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/Luna.png',
    breed: '',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 4,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 5,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  }
]

@Component({
  selector: 'app-calendar-main',
  templateUrl: './calendar-main.component.html',
  styleUrls: ['./calendar-main.component.css']
})

export class CalendarMainComponent implements OnInit {
  public dogs: Array<any>;

  constructor() { }

  ngOnInit() {
    this.dogs = todaysDogs;
  }

}
