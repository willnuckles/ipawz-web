import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

const allDogs = [
  {
    id: 1,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 2,
    name: 'Cash',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/cash.png',
    breed: 'Bulldog',
    medicalNeeds: 'Hugs',
    userId: 1
  },
  {
    id: 3,
    name: 'Sharkie',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/sharkie.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 4,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 5,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 6,
    name: 'Cash',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/cash.png',
    breed: 'Bulldog',
    medicalNeeds: 'Hugs',
    userId: 1
  },
  {
    id: 7,
    name: 'Sharkie',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/sharkie.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 8,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 9,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 10,
    name: 'Cash',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/cash.png',
    breed: 'Bulldog',
    medicalNeeds: 'Hugs',
    userId: 1
  },
  {
    id: 11,
    name: 'Sharkie',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/sharkie.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 12,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 13,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 14,
    name: 'Cash',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/cash.png',
    breed: 'Bulldog',
    medicalNeeds: 'Hugs',
    userId: 1
  },
  {
    id: 15,
    name: 'Sharkie',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/sharkie.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 16,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 17,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 18,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 19,
    name: 'Cash',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/cash.png',
    breed: 'Bulldog',
    medicalNeeds: 'Hugs',
    userId: 1
  },
  {
    id: 20,
    name: 'Sharkie',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/sharkie.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  },
  {
    id: 21,
    name: 'Nola Blue',
    imgUrl: 'https://www.iseatz.com/assets/about/dogs/nola-blu.png',
    breed: 'Bulldog',
    medicalNeeds: 'None',
    userId: 1
  }
]

@Component({
  selector: 'app-our-dogs',
  templateUrl: './our-dogs.component.html',
  styleUrls: ['./our-dogs.component.css']
})
export class OurDogsComponent implements OnInit {
  public dogs: Object;
  constructor(private http: HttpClient) { }

  configUrl = 'http://localhost:3000/dogs';

  getConfig() {
    return this.http.get(this.configUrl);
  }

  ngOnInit() {
    let results = this.getConfig();
    console.log('results', results);
    this.dogs = allDogs;
  }

}
