import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { CalendarMainComponent } from './calendar-main/calendar-main.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DogCardComponent } from './dog-card/dog-card.component';
import {MatButtonModule, MatCheckboxModule} from '@angular/material';
import {MatCardModule} from '@angular/material/card';
import { RouterModule, Routes } from '@angular/router';
import { LandingComponent } from './landing/landing.component';
import { OurDogsComponent } from './our-dogs/our-dogs.component';
import { HttpClientModule } from '@angular/common/http';



const appRoutes: Routes = [
  { path: '', component: LandingComponent },
  { path: 'todays-puppers', component: CalendarMainComponent },
  { path: 'our-dogs', component: OurDogsComponent},
  { path: '**', component: LandingComponent}
];
@NgModule({
  declarations: [
    AppComponent,
    CalendarMainComponent,
    DogCardComponent,
    LandingComponent,
    OurDogsComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatButtonModule,
    MatCheckboxModule,
    MatCardModule,
    RouterModule,
    RouterModule.forRoot(
      appRoutes
    ),
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
